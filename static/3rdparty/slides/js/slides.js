$(window).load(function(){
	var pagesOld = $('.p1 li');
	var pages = new Array();
	var currentPage = new Array();
	var current = new Array();
	var nextPage = new Array();

	$('.slides').each(function() {
		var id = this.id;
		pages[id] = document.getElementById(id).getElementsByTagName('li');
		current[id] = 0;
	});

	$('.slides .button').click(function(event){
		var id = event.target.parentElement.id;
		currentPage[id] = $(pages[id]).eq(current[id]);
		if($(this).hasClass('prevButton'))
		{
			if (current[id] <= 0)
				current[id]=$(pages[id]).length-1;
			else
				current[id]=current[id]-1;
		}
		else
		{
			if (current[id] >= $(pages[id]).length-1)
				current[id]=0;
			else
				current[id]=current[id]+1;
		}
		nextPage[id] = $(pages[id]).eq(current[id]);
		currentPage[id].hide();
		nextPage[id].show();
	});

});


